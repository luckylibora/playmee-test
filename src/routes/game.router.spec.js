const request = require('supertest');
const app = require('../app');

describe('GameRouter', () => {
  describe('POST /start', () => {
    it('should respond with game id', async () => {
      const res = await request(app)
        .post('/start')
        .send({
          grid_size: {
            x: 10,
            y: 10,
          },
          bomb_quantity: 10,
        })
        .expect(200);
      expect(res.body)
        .toHaveProperty('game_id');
    });
  });

  describe('POST /select', () => {
    it('should respond with changed cells list', async () => {
      const startRes = await request(app)
        .post('/start')
        .send({
          grid_size: {
            x: 10,
            y: 10,
          },
          bomb_quantity: 10,
        })
        .expect(200);
      const gameId = startRes.body.game_id;
      const selectRes = await request(app)
        .post('/select')
        .send({
          game_id: gameId,
          grid_position: {
            x: 0,
            y: 0,
          },
        })
        .expect(200);
      expect(selectRes.body)
        .toHaveProperty('results');
      expect(selectRes.body.results)
        .toBeInstanceOf(Array);
      const cell = selectRes.body.results[0];
      expect(cell)
        .toHaveProperty('x');
      expect(cell)
        .toHaveProperty('y');
      expect(cell)
        .toHaveProperty('value');
    });
  });
});
