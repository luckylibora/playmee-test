const express = require('express');
const { gameService } = require('../services/game.service');

const gameRouter = express.Router();

gameRouter.post('/start', (req, res) => {
  const { grid_size: gridSize, bomb_quantity: bombQuantity } = req.body;
  const { x, y } = gridSize;

  const game = gameService.create({
    width: x,
    height: y,
    bombQuantity,
  });

  res.send({
    game_id: game.id,
  });
});

gameRouter.post('/select', (req, res) => {
  const { game_id: id, grid_position: position } = req.body;
  const { x, y } = position;

  const changedCells = gameService.select({
    id,
    x,
    y,
  });
  res.send({ results: changedCells });
});

module.exports = { gameRouter };
