const express = require('express');
const { gameRouter } = require('./game.router');

const indexRouter = express.Router();

indexRouter.use('/', gameRouter);

module.exports = { indexRouter };
