const _ = require('lodash');
const Cell = require('./cell.model');

const GameInProgress = 0;
const GameLost = 1;
const GameWon = 2;

const BombCellValue = -1;

/**
 * @typedef {{x: number, y: number, value: number}[]} ChangedCellsList
 */

/**
 *
 * @property {string} id?
 * @property {number} width
 * @property {number} height
 * @property {number} bombQuantity
 * @property {number} status
 * @property {Cell[][]} cellMatrix
 */
class Game {
  get isFinished() {
    return this.status !== GameInProgress;
  }

  /**
   *
   * @param {number} width
   * @param {number} height
   * @param {number} bombQuantity
   */
  constructor({
    width, height, bombQuantity,
  }) {
    if (width * height <= bombQuantity) {
      throw new Error('Too many bombs for selected grid size');
    }

    this.width = width;
    this.height = height;
    this.bombQuantity = bombQuantity;
    this.status = GameInProgress;
    this.cellMatrix = [];
  }

  /**
   *
   * @returns {void}
   */
  initialize() {
    for (let i = 0; i < this.width; i++) {
      this.cellMatrix.push([]);
      for (let j = 0; j < this.height; j++) {
        this.cellMatrix[i][j] = new Cell({
          isBomb: false,
          value: null,
        });
      }
    }

    for (let i = 0; i < this.bombQuantity; i++) {
      this._addBomb();
    }
  }

  /**
   *
   * @param {number} x
   * @param {number} y
   * @returns {ChangedCellsList}
   */
  selectCell({ x, y }) {
    if (this.isFinished) {
      throw new Error('game is already finished');
    }

    const cell = this.cellMatrix[x][y];
    if (cell.isOpened) {
      throw new Error('this cell is already opened');
    }

    if (cell.isBomb) {
      this.status = GameLost;
      cell.value = BombCellValue;
      return [{
        x,
        y,
        value: BombCellValue,
      }];
    }

    const changedCells = this._openCell({
      x,
      y,
    });
    const isWon = this._checkIsWon();
    if (isWon) {
      this.status = GameWon;
    }
    return changedCells;
  }

  /**
   *
   * @private
   * @returns {void}
   */
  _addBomb() {
    const isAdded = false;
    do {
      const cell = this._getRandomCell();

      if (!cell.isBomb) {
        cell.isBomb = true;
        return;
      }
    } while (!isAdded);
  }

  /**
   *
   * @private
   * @returns {boolean}
   */
  _checkIsWon() {
    for (let i = 0; i < this.width; i++) {
      for (let j = 0; j < this.height; j++) {
        const cell = this.cellMatrix[i][j];
        if (!cell.isBomb && !cell.isOpened) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   *
   * @param {number} x
   * @param {number} y
   * @returns {{cell:Cell, x: number, y: number}[]}
   * @private
   */
  _getCellNeighbours({ x, y }) {
    const neighbours = [];
    for (let shiftX = -1; shiftX <= 1; shiftX++) {
      for (let shiftY = -1; shiftY <= 1; shiftY++) {
        const shiftedX = x + shiftX;
        if (shiftedX < 0 || shiftedX >= this.width) {
          continue;
        }

        const shiftedY = y + shiftY;
        if (shiftedY < 0 || shiftedY >= this.height) {
          continue;
        }

        neighbours.push({
          x: shiftedX,
          y: shiftedY,
          cell: this.cellMatrix[shiftedX][shiftedY],
        });
      }
    }
    return neighbours;
  }

  /**
   *
   * @returns {Cell}
   * @private
   */
  _getRandomCell() {
    const x = _.random(0, this.width - 1);
    const y = _.random(0, this.height - 1);
    return this.cellMatrix[x][y];
  }

  /**
   *
   * @param {number} x
   * @param {number} y
   * @private
   * @returns {ChangedCellsList}
   */
  _openCell({ x, y }) {
    const cell = this.cellMatrix[x][y];
    const neighbours = this._getCellNeighbours({
      x,
      y,
    });
    const value = neighbours.filter((neighbour) => neighbour.cell.isBomb).length;
    cell.value = value;
    const changedCells = [{
      x,
      y,
      value,
    }];
    if (value > 0) {
      return changedCells;
    }

    return neighbours
      .filter((neighbour) => !neighbour.cell.isBomb && !neighbour.cell.isOpened)
      .map((neighbour) => this._openCell({
        x: neighbour.x,
        y: neighbour.y,
      }))
      .reduce((prev, curr) => [...prev, ...curr], changedCells);
  }
}

module.exports = {
  Game,
  GameInProgress,
  GameWon,
  GameLost,
};
