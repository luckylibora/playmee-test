const { GameWon, Game, GameLost } = require('./game.model');

describe('Game', () => {
  describe('#constructor', () => {
    it('should throw error', () => {
      expect(() => new Game({
        width: 2,
        height: 2,
        bombQuantity: 4,
      }))
        .toThrow();
    });
  });

  describe('#initialize', () => {
    it('should fill game with data', () => {
      const game = new Game({
        width: 10,
        height: 5,
        bombQuantity: 6,
      });
      game.initialize();
      expect(game.cellMatrix)
        .toHaveLength(10);
      expect(game.cellMatrix[0])
        .toHaveLength(5);

      let addedBombAmount = 0;
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 5; j++) {
          if (game.cellMatrix[i][j].isBomb) {
            addedBombAmount++;
          }
        }
      }
      expect(addedBombAmount)
        .toBe(6);
    });
  });

  describe('#openCell', () => {
    it('should return opened cells', () => {
      const game = new Game({
        width: 10,
        height: 10,
        bombQuantity: 10,
      });
      game.initialize();
      const res = game.selectCell({
        x: 0,
        y: 0,
      });
      expect(res)
        .toBeInstanceOf(Array);
      expect(res[0])
        .toHaveProperty('x');
      expect(res[0])
        .toHaveProperty('y');
      expect(res[0])
        .toHaveProperty('value');
    });

    it('should change status to GameWon', () => {
      const game = new Game({
        width: 10,
        height: 10,
        bombQuantity: 0,
      });
      game.initialize();
      game.selectCell({
        x: 0,
        y: 0,
      });
      expect(game.status)
        .toBe(GameWon);
    });

    it('should throw error for finished game', () => {
      const game = new Game({
        width: 10,
        height: 10,
        bombQuantity: 10,
      });
      game.status = GameWon;
      expect(() => game.selectCell({
        x: 0,
        y: 0,
      }))
        .toThrow();
    });

    it('should throw error if selected cell has been already opened', () => {
      const game = new Game({
        width: 10,
        height: 10,
        bombQuantity: 0,
      });
      game.initialize();
      game.cellMatrix[0][0].value = 0;
      expect(() => game.selectCell({
        x: 0,
        y: 0,
      }))
        .toThrow();
    });

    it('should finish game with GameLost status', () => {
      const game = new Game({
        width: 10,
        height: 10,
        bombQuantity: 1,
      });
      game.initialize();
      game.cellMatrix[0][0].isBomb = true;
      game.selectCell({
        x: 0,
        y: 0,
      });
      expect(game.status)
        .toBe(GameLost);
    });
  });
});
