/**
 *
 * @property {number|null} value
 * @property {boolean} isBomb
 */
class Cell {
  get isOpened() {
    return this.value !== null;
  }

  /**
   *
   * @param {boolean} isBomb
   * @param {number|null} value
   */
  constructor({ isBomb, value }) {
    this.value = value;
    this.isBomb = isBomb;
  }
}

module.exports = Cell;
