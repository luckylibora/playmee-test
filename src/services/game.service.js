const { gameRepository } = require('../repositories/game.repository');
const { Game } = require('../models/game.model');

class GameService {
  /**
   *
   * @param {number} width
   * @param {number} height
   * @param {number} bombQuantity
   * @returns {Game}
   */
  create({ width, height, bombQuantity }) {
    const game = new Game({
      width,
      height,
      bombQuantity,
    });
    game.initialize();
    gameRepository.insert(game);
    return game;
  }

  /**
   *
   * @param {string} id
   * @param {number} x
   * @param {number} y
   * @returns {Game}
   */
  select({ id, x, y }) {
    const game = gameRepository.findById(id);
    if (!game) {
      throw new Error('Game not found');
    }

    return game.selectCell({
      x,
      y,
    });
  }
}

const gameService = new GameService();

module.exports = { gameService };
