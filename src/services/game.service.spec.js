const { gameRepository } = require('../repositories/game.repository');
const { gameService } = require('./game.service');

describe('GameService', () => {
  describe('#create', () => {
    it('should create new game and save it', () => {
      const game = gameService.create({
        width: 10,
        height: 10,
        bombQuantity: 10,
      });
      expect(gameRepository.findById(game.id))
        .toBe(game);
    });
  });

  describe('#select', () => {
    it('should return changed cells list for game', () => {
      const game = gameService.create({
        width: 10,
        height: 10,
        bombQuantity: 10,
      });
      const res = gameService.select({
        id: game.id,
        x: 0,
        y: 0,
      });
      expect(res)
        .toBeInstanceOf(Array);
    });

    it('should throw error for non-existing game', () => {
      expect(() => gameService.select({
        id: '0',
        x: 0,
        y: 0,
      }))
        .toThrow();
    });
  });
});
