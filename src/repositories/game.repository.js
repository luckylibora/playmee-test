const uuid = require('uuid');

class GameRepository {
  constructor() {
    this.gameMap = new Map();
  }

  /**
   *
   * @param {Game} game
   */
  insert(game) {
    const id = uuid.v4();
    game.id = id;
    this.gameMap.set(id, game);
  }

  /**
   *
   * @param {string} id
   * @returns {Game}
   */
  findById(id) {
    return this.gameMap.get(id);
  }
}

const gameRepository = new GameRepository();

module.exports = { gameRepository };
