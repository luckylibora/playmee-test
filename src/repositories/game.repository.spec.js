const { gameRepository } = require('./game.repository');
const { Game } = require('../models/game.model');

describe('GameRepository', () => {
  it('should insert new game and receive it by generated id', () => {
    const game = new Game({
      width: 10,
      height: 10,
      bombQuantity: 10,
    });
    gameRepository.insert(game);
    expect(gameRepository.findById(game.id))
      .toBe(game);
  });
});
